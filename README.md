### Prerequisites

* Basic knowledge of Electronics
* Basic knowledge of PCB designing using Eagle

### Course Content

#### Day 1

* Eagle Libraries
* Creating custom components in Eagle
* Adding custom components in a design

#### Day 2

* What is ULP
* Top 10 useful ULPs

#### Day 3

* Introduction to Eagle 3D with Fusion 360
* How to create repository in Fusion 360

#### Day 4

* How to insert 3D Packages in Eagle
* Preparing custom libray with 3D packages

#### Day 5

* Finalize a design in Fusion 360
* Rendering a 3D design